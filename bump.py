import json

def main():
    str = ""
    with open('plugin.json','r') as f:
        str = f.read()
        f.close()

    with open('plugin.json','w') as f:
        jsonVer = json.loads(str)
        jsonVer["version"] = float(jsonVer["version"]) + 0.1
        jsonVer["version"] = "{:.1f}".format(jsonVer["version"])
        json.dump(jsonVer, f,indent=4)

if __name__ == '__main__':
    main()