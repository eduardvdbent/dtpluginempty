import json
import logging
import subprocess
from ruxit.api.base_plugin import BasePlugin
from ruxit.api.snapshot import pgi_name


class ProcessNumber(BasePlugin):
    def query(self, **kwargs):
        pgi = self.find_single_process_group(pgi_name('plugin_sdk.demo_app'))
        pgi_id = pgi.group_instance_id
        stats =  int(subprocess.run(" ps -A --no-headers | wc -l", shell=True, stdout=subprocess.PIPE).stdout.decode('ascii').strip())
        self.results_builder.absolute(key='processnum1234', value=stats, entity_id=pgi_id)